package com.example.exam6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IOnChildItemClick{
    private List<ContactModel> listcontacts = new ArrayList<>();
    private ListView lvContact;
    private ContactAdapter nAdapter;
    private ImageView ivUser;
    private TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        initView();
        nAdapter = new ContactAdapter(this,listcontacts);
        nAdapter.registerChildItemClick(this);
        lvContact.setAdapter(nAdapter);
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ContactModel model = listcontacts.get(i);
                Toast.makeText(MainActivity.this,model.getName()+"i"+model.getPhone(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        lvContact = (ListView)findViewById(R.id.lvcontact);
        ivUser = (ImageView)findViewById(R.id.ivUser);
        tvName = (TextView)findViewById(R.id.tvName);
    }

    private void initData() {
        listcontacts.add(new ContactModel("tran hung", "015679842",R.drawable.ic_launcher_background));
        listcontacts.add(new ContactModel("nguyen thi hoa", "7845158",R.drawable.ic_launcher_foreground));
        listcontacts.add(new ContactModel("ta van trung", "546134565",R.drawable.ic_launcher_foreground));
        listcontacts.add(new ContactModel("le thi huong", "57931442",R.drawable.ic_launcher_foreground));
        listcontacts.add(new ContactModel("bao the ngoc", "1234567890",R.drawable.ic_launcher_foreground));
        listcontacts.add(new ContactModel("join smith", "55551211",R.drawable.ic_launcher_foreground));
        listcontacts.add(new ContactModel("shiba tatsuya", "012357964",R.drawable.ic_launcher_foreground));


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        nAdapter.unRegisterChildItemClick();
    }


    @Override
    public void onItemChildItemClick(int podision) {
        ContactModel contact = listcontacts.get(podision);
        ivUser.setImageResource(contact.getImage());
        tvName.setText(contact.getName());

    }
}