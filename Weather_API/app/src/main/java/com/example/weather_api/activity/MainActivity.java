package com.example.weather_api.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.weather_api.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}