package com.example.practical;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{

    private EditText edName;
    private EditText edEmail;
    private EditText edComment;
    private Button btSend;
    private Spinner spinner;
    private DBHelper db;
    private CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        db = new DBHelper(this);
        db.getReadableDatabase();
    }

    private void initView() {
        edName = (EditText) findViewById(R.id.edName);
        edEmail = (EditText) findViewById(R.id.edEmail);
        btSend = (Button) findViewById(R.id.btSend);
        checkBox = (CheckBox) findViewById(R.id.ck);
        edComment = (EditText) findViewById(R.id.edComment);
        btSend.setOnClickListener(this);

        String[] types = {"gripe" , "handhold", "unknow"};
        spinner = (Spinner) findViewById(R.id.spiner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> paren, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> paren) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == btSend){
            onAdd();
        }
    }

    private void onAdd() {
        if (edName.getText().toString().isEmpty()){
            Toast.makeText(this,"please enter name",Toast.LENGTH_LONG).show();
            return;
        }
        if (edEmail.getText().toString().isEmpty()){
            Toast.makeText(this,"please enter email",Toast.LENGTH_LONG).show();
            return;
        }
        String isAdd  = db.addUser(edName.getText().toString(), spinner.getSelectedItem().toString(),
                edEmail.getText().toString(),edComment.getText().toString());
        Toast.makeText(this,isAdd,Toast.LENGTH_LONG).show();
        Intent intent = new Intent(MainActivity.this, ListUserAct.class);
        startActivity(intent);
    }
}