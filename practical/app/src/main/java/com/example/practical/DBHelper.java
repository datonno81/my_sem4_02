package com.example.practical;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "USER";
    public static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "TBL_USER";
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String EMAIL = "email";
    public static final String COMMENT = "comment";

    public DBHelper(Context context) {
        super(context, DB_NAME,null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE "+ TABLE_NAME + "(" + ID +
                " INTEGER PRIMARY KEY, " +NAME+ " TEXT, " +
                TYPE + " TEXT, "+
                EMAIL + " TEXT,"+
                COMMENT + " TEXT )";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " +TABLE_NAME;
        db.execSQL(sql);
        onCreate(db);
    }

    public String addUser(String user, String type, String email ,String comment){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME,user);
        contentValues.put(TYPE,type);
        contentValues.put(EMAIL,email);
        contentValues.put(COMMENT,comment);
        long isAdd = db.insert(TABLE_NAME,null,contentValues);
        if (isAdd == -1){
            return "Add fail";
        }
        db.close();
        return "Add success";
    }
    public String updateUser(int id , String user, String type, String email, String comment){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME,user);
        contentValues.put(TYPE,type);
        contentValues.put(EMAIL,email);
        contentValues.put(COMMENT,comment);
        int isUpdate = db.update(TABLE_NAME,contentValues,ID +" = ? ",new String[]{id+""});
        if (isUpdate > 0){
            return "Update success";
        }
        db.close();
        return "Update fail";
    }
    public Cursor getAllUser(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " +TABLE_NAME;
        Cursor c = db.rawQuery(sql,null);
        return c;
    }
}
