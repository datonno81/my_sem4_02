package com.example.sqlite.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.sqlite.R;
import com.example.sqlite.database.DatabaseHelper;
import com.example.sqlite.model.NewItem;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DatabaseHelper db = new DatabaseHelper(this);
        db.getWritableDatabase();

        NewItem itemUpdate = new NewItem(1,"this 1 update","content 1 update", "description 1 update");
        db.update(itemUpdate);

        List<NewItem> list =  db.getData();
        for (NewItem item : list){
            Log.d("TAG", item.getTitle());
        }
    }
}