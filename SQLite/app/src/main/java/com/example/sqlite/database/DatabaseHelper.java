package com.example.sqlite.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sqlite.model.NewItem;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "NEWS";
    private static int    DB_VERSION = 1;
    private static String TABLE = "TBL_BOOKMARK";

    public DatabaseHelper( Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        //create bookmark
        //id integer primary key
        //title text ...

        String sql = "CREATE TABLE " + TABLE + "("+
                BOOKMARK.id + "INTERER PRIMARY KEY," +
                BOOKMARK.title + "TEXT," +
                BOOKMARK.content + "TEXT," +
                BOOKMARK.desciption + "TEXT" +")";
        db.execSQL(sql);

    }
    public void addData(NewItem item){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BOOKMARK.id,item.getId());
        contentValues.put(BOOKMARK.title,item.getTitle());
        contentValues.put(BOOKMARK.content,item.getContent());
        contentValues.put(BOOKMARK.desciption,item.getDescription());
        db.insert(TABLE,null,contentValues);

    }
    public List<NewItem> getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT * FROM " +TABLE;
        Cursor cursor = db.rawQuery(sql,null);
        List<NewItem> listItem = new ArrayList<>();

        if (cursor.getCount() > 0){
            cursor.moveToFirst();
            do {
                NewItem item = new NewItem();
                item.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(BOOKMARK.id))));
                item.setTitle(cursor.getString(cursor.getColumnIndex(BOOKMARK.title)));
                item.setContent(cursor.getString(cursor.getColumnIndex(BOOKMARK.content)));
                item.setDescription(cursor.getString(cursor.getColumnIndex(BOOKMARK.desciption)));
                listItem.add(item);

            }while (cursor.moveToNext());
        }
        return listItem;
    }

    public void delete(NewItem item){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE,BOOKMARK.id + " = ?" ,new String[]{String.valueOf(item.getId())});
    }
    public void update(NewItem item){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BOOKMARK.title,item.getTitle());
        contentValues.put(BOOKMARK.content,item.getContent());
        contentValues.put(BOOKMARK.desciption,item.getDescription());

        db.update(TABLE,contentValues,BOOKMARK.id + "= ?",new String[]{String.valueOf(item.getId())});
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVerSion, int newVersion) {

    }
}
