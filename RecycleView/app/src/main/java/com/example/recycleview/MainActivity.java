package com.example.recycleview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ProductAdapter.IOnClickItem{
    List<Product> listproduct = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        ProductAdapter adapter = new ProductAdapter(this,listproduct,this);

        GridLayoutManager layoutManager = new GridLayoutManager(this,2);

        RecyclerView rvProduct = (RecyclerView) findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);
    }

    private void initData() {
        listproduct.add(new Product("zara ? p1","blue under black","110.122.000",R.drawable.p1));
        listproduct.add(new Product("zara ? p2","orange","150.122.000",R.drawable.p2));
        listproduct.add(new Product("zara ? p3","midori","541.354.000",R.drawable.p3));

    }

    @Override
    public void OnclickItem(int position) {
        Product product = listproduct.get(position);
        Toast.makeText(this,product.getTitle(),Toast.LENGTH_SHORT).show();

    }
}