package com.example.recycleview;

public class Product {
    private String title;
    private String des;
    private String price;
    private int img;

    public Product(String title, String des, String price, int img) {
        this.title = title;
        this.des = des;
        this.price = price;
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public String getDes() {
        return des;
    }

    public String getPrice() {
        return price;
    }

    public int getImg() {
        return img;
    }
}
