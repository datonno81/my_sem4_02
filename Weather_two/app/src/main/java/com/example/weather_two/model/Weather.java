package com.example.weather_two.model;

public class Weather {
    private String DateTime;
    private int WeatherIcon;
    private String IconPhrase;
    private Temperature temperature;

    public String getDateTime() {
        return DateTime;
    }

    public int getWeatherIcon() {
        return WeatherIcon;
    }

    public String getIconPhrase() {
        return IconPhrase;
    }

    public com.example.weather_two.model.Temperature getTemperature() {
        return temperature;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public void setWeatherIcon(int weatherIcon) {
        WeatherIcon = weatherIcon;
    }

    public void setIconPhrase(String iconPhrase) {
        IconPhrase = iconPhrase;
    }

    public void setTemperature(com.example.weather_two.model.Temperature temperature) {
        this.temperature = temperature;
    }
}
