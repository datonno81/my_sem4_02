package com.example.network.model;

public class Content {
    private String description;
    private String url;

    public Content(String description, String url) {
        this.description = description;
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }
}
