package com.example.network;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.network.adapter.NewsAdapter;

public class ListNewsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_news);
        //b1 Data source
        getData();

        //b2 Adapter
        adapter new NewsAdapter(this,listData)


    }

    private void getData() {
    }

}
