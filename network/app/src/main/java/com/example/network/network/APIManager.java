package com.example.network.network;

import com.example.network.model.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIManager {
    String SEVER_URL = "http://api-demo-anhth.herokuapp.com/";

    @GET("data.json")
    Call<Item> getItemData();

    @GET("data.json")
    Call<List<Item>> getListData();
}
