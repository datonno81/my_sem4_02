package com.example.network.model;

public class Item {
    private int id;
    private String title;
    private String date;
    private Content content;
    private String image;

    public Item(int id, String title, String date, Content content, String image) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.content = content;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public Content getContent() {
        return content;
    }

    public String getImage() {
        return image;
    }
}
