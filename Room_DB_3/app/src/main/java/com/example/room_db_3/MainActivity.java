package com.example.room_db_3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.room_db_3.database.AppDatabase;
import com.example.room_db_3.database.BookMarkEntity;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = AppDatabase.getAppDatabase(this);
        insertBookMark();
        updateBookMark(2);
        getAllBookMark();
        findBookMark(1);
    }


    private void updateBookMark(int id) {
        BookMarkEntity bm = db.bookMarkDao().getBookMark(id);
        bm.title = "this is update title";
        db.bookMarkDao().updateBookMark(bm);
    }

    private void insertBookMark() {
        BookMarkEntity bm = new BookMarkEntity();
        bm.title = "this is title";
        bm.content = "this is content";
        db.bookMarkDao().insertBookMark(bm);
    }

    private void findBookMark(int id) {
        BookMarkEntity model = db.bookMarkDao().getBookMark(id);
        Log.d("Tag", "find BookMark with id : " + model.id + "title" + model.title);
    }

    private void deleteBookMark(int id) {
        BookMarkEntity model = db.bookMarkDao().getBookMark(id);
        db.bookMarkDao().deleteBookMark(model);
    }

    private void deleteAllBookMark() {
        db.bookMarkDao().deleteAll();
    }

    private void getAllBookMark() {
        List<BookMarkEntity> listBookMark = db.bookMarkDao().getAllBookMark();
        for (BookMarkEntity model : listBookMark) {
            Log.d("Tag", "find BookMark with id : " + model.id + "title" + model.title);
        }
    }
}