package com.example.room_db_3.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface BookMarkDao {
    @Insert(onConflict = REPLACE)
    void insertBookMark(BookMarkEntity bookmark);

    @Update
    void updateBookMark(BookMarkEntity bookmark);

    @Delete
    void  deleteBookMark(BookMarkEntity bookmark);

    @Query("SELECt * FROM BookMark")
    List<BookMarkEntity> getAllBookMark();

    @Query("SELECT * FROM BookMark WHERE id = :id")
    BookMarkEntity getBookMark(int id);

    @Query("DELETE FROM BookMark")
    void deleteAll();

}
